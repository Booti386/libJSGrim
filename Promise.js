(function() {

let PromiseState = function() {}
PromiseState.PENDING = 0;
PromiseState.FULFILLED = 1;
PromiseState.REJECTED = 2;

let PromiseThen = function(parent, child, callback, err_callback)
{
	this.parent = parent;
	this.child = child;
	this.callback = callback;
	this.err_callback = err_callback;
}

PromiseThen.prototype.exec = function()
{
	let callback = this.callback ? this.callback : (val) => val;
	let err_callback = this.err_callback ? this.err_callback : (val) => val;

	callback = this.parent.state === PromiseState.FULFILLED ? callback : err_callback;

	if (this.err_callback)
		this.parent.reject_was_handled = true;

	if (this.parent.value instanceof Promise)
	{
		this.parent = this.parent.value;

		if (this.parent.state === PromiseState.PENDING)
			this.parent.pending_then.push(this);
		else
			this.exec();
		return;
	}

	try
	{
		this.child.value = callback(this.parent.value);
	}
	catch (ex)
	{
		this.child.state = PromiseState.REJECTED;
		this.child.value = ex;
	}

	if (this.child.state === PromiseState.PENDING)
		this.child.state = PromiseState.FULFILLED;

	for (let idx in this.child.pending_then)
	{
		if (!this.child.pending_then.hasOwnProperty(idx))
			continue;

		this.child.pending_then[idx].exec();
	}

	if (this.child.state === PromiseState.REJECTED && !this.child.reject_was_handled)
		throw this.child.value;
}

let Promise = function(resolve_callback)
{
	this.state = PromiseState.PENDING;
	this.value = null;
	this.pending_then = [];
	this.reject_was_handled = false;

	let handle_ret = () => {
		for (let idx in this.pending_then)
		{
			if (!this.pending_then.hasOwnProperty(idx))
				continue;

			this.pending_then[idx].exec();
		}

		if (this.state === PromiseState.REJECTED && !this.reject_was_handled)
			throw this.value;
	};

	try
	{
		resolve_callback((val) => {
			window.setTimeout(() => {
				this.state = PromiseState.FULFILLED;
				this.value = val;
				handle_ret();
			}, 0);
		}, (reason) => {
			window.setTimeout(() => {
				this.state = PromiseState.REJECTED;
				this.value = reason;
				handle_ret();
			}, 0);
		});
	}
	catch (ex)
	{
		window.setTimeout(() => {
			this.state = PromiseState.REJECTED;
			this.value = ex;
			handle_ret();
		}, 0);
	}
}

Promise.resolve = function(val)
{
	return new Promise((resolve, reject) => resolve(val));
}

Promise.reject = function(reason)
{
	return new Promise((resolve, reject) => reject(reason));
}

Promise.all = function(deps)
{
	let cnt = 0;
	let result = [];
	let rejected = false;

	if (deps.length === 0)
		return Promise.resolve([]);

	return new Promise((resolve, reject) => {
		for (let i in deps)
		{
			let dep = deps[i];

			if (!(dep instanceof Promise))
				dep = Promise.resolve(dep);

			dep.then((val) => {
				result[i] = val;
				cnt++;

				if (cnt === deps.length)
					resolve(result);
			}, (reason) => {
				if (rejected)
					return;

				rejected = true;
				reject(reason);
			});
		}
	});
}

Promise.prototype.then = function(callback, err_callback)
{
	let child = new Promise(() => {});
	let block_then = new PromiseThen(this, child, callback, err_callback);

	if (this.state !== PromiseState.PENDING)
	{
		window.setTimeout(() => {
			block_then.exec();
		}, 0);
		return child;
	}

	this.pending_then.push(block_then);
	return child;
}

Promise.prototype.catch = function(err_callback)
{
	return this.then(null, err_callback);
}

Grim.Promise = (typeof window.Promise === 'undefined') ? Promise : window.Promise;

})();

