(function() {

/* SVG */

let SVG = function() {};

SVG.createElement = function(tag_name)
{
	return document.createElementNS('http://www.w3.org/2000/svg', tag_name);
}

SVG.createSVGElement = function()
{
	return SVG.createElement('svg');
}

SVG.strToSVGElement = function(str)
{
	let div = document.createElement('div');
	let svg;

	div.innerHTML = str;
	svg = div.getElementsByTagName('svg')[0];

	if (svg === undefined || svg === null)
		throw 'Malformed SVG';

	return svg;
}

/* SVGSVGElement */

let SVGElement = function() {}

SVGElement.setRect = function(elem, x, y, w, h)
{
	elem.setAttribute('x', x);
	elem.setAttribute('y', y);
	elem.setAttribute('width', w);
	elem.setAttribute('height', h);
}

/* Exports */

SVG.SVGElement = SVGElement;
Grim.SVG = SVG;

})();

