(function() {

/* Imports */

let Promise = Grim.Promise;

/* Utils */

let Utils = function() {};

Utils.getMouseRelativePos = function(elem, evt)
{
	let rect = elem.getBoundingClientRect();

	return {
		x: (evt.clientX - rect.left) / rect.width,
		y: (evt.clientY - rect.top) / rect.height
	};
}

Utils.getTimestamp = function()
{
	return new Date().getTime();
}

Utils.loadImg = function(url)
{
	let img = new Image();

	return new Promise((resolve, reject) => {
		img.addEventListener('load', () => resolve(img));
		img.addEventListener('error', () => reject('Loading of image "' + img.src + '" failed.'));
		img.addEventListener('abort', () => reject('Loading of image "' + img.src + '" aborted.'));
		img.src = url;
	});
}

Utils.loadImgAuto = function(url)
{
	return Utils.loadImg(url).then((img) => {
		img.width = img.naturalWidth;
		img.height = img.naturalHeight;

		return img;
	});
}

Utils.loadVideo = function(url, attrs)
{
	let video = document.createElement('video');
	let wait_complete_load = false;

	for (let i in attrs)
	{
		let attr = attrs[i];

		if (!attrs.hasOwnProperty(i))
			continue;

		if (i === 'Grim.WaitCompleteLoad')
		{
			wait_complete_load = attr;
			continue;
		}

		video.setAttribute(i, attr);
	}

	return new Promise((resolve, reject) => {
		video.addEventListener(wait_complete_load ? 'canplaythrough' : 'canplay', () => resolve(video));
		video.addEventListener('error', () => reject('Loading of video "' + video.src + '" failed.'));
		video.addEventListener('abort', () => reject('Loading of video "' + video.src + '" aborted.'));
		video.src = url;
	});
}

Utils.loadVideoAuto = function(url, attrs)
{
	return Utils.loadVideo(url, attrs).then((video) => {
		video.width = video.videoWidth;
		video.height = video.videoHeight;

		return video;
	});
}

Utils.disableDragDrop = function(elem)
{
	let handler = (evt) => {
		evt.preventDefault();
	};

	elem.addEventListener('drag', handler);
	elem.addEventListener('dragstart', handler);
	elem.addEventListener('dragover', handler);
	elem.addEventListener('dragleave', handler);
	elem.addEventListener('dragexit', handler);
	elem.addEventListener('dragenter', handler);
	elem.addEventListener('dragend', handler);
}

Utils.disableUserSelect = function(elem)
{
	let style = elem.getAttribute('style');

	if (style === undefined || style === null)
		style = '';

	style += '-webkit-touch-callout: none;';
	style += '-webkit-user-select: none;';
	style += '-khtml-user-select: none;';
	style += '-moz-user-select: none;';
	style += '-ms-user-select: none;';
	style += '-o-user-select: none;';
	style += 'user-select: none;';

	elem.setAttribute('style', style);
}

Utils.requestPointerLock = function(elem)
{
	if (elem.requestPointerLock)
	{
		elem.requestPointerLock();
		return;
	}
	
	if (elem.setCapture)
	{
		elem.setCapture(false);
		return;
	}

	console.log('libJSGrim: Utils.requestPointerLock() not implemented.');
};

Utils.exitPointerLock = function()
{
	if (document.exitPointerLock)
	{
		document.exitPointerLock();
		return;
	}
	
	if (document.releaseCapture)
	{
		document.releaseCapture();
		return;
	}

	console.log('libJSGrim: Utils.exitPointerLock() not implemented.');
};

Utils.updateStyle = function(elem, key, val)
{
	let style = elem.getAttribute('style');
	let found = false;

	if (!style)
		style = '';

	let props_arr = style.split(';');
	for (let i in props_arr)
	{
		let prop = props_arr[i];

		if (!props_arr.hasOwnProperty(i))
			continue;

		let prop_key = prop.split(':', 2)[0];
		if (prop_key !== key)
			continue;

		props_arr[i] = key + ':' + val;
		found = true;
		break;
	}

	style = props_arr.join(';');

	if (!found)
	{
		style = style.trim();

		if (style.length > 0
				&& style[style.length - 1] !== ';')
			style += ';';

		style += key + ':' + val + ';';
	}

	elem.setAttribute('style', style);
}

/* Exports */

Grim.Utils = Utils;

})();

