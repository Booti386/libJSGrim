'use strict';

function Grim() {}

(function() {

let PromiseState = function() {}
PromiseState.PENDING = 0;
PromiseState.FULFILLED = 1;
PromiseState.REJECTED = 2;

let PromiseThen = function(parent, child, callback, err_callback)
{
	this.parent = parent;
	this.child = child;
	this.callback = callback;
	this.err_callback = err_callback;
}

PromiseThen.prototype.exec = function()
{
	let callback = this.callback ? this.callback : (val) => val;
	let err_callback = this.err_callback ? this.err_callback : (val) => val;

	callback = this.parent.state === PromiseState.FULFILLED ? callback : err_callback;

	if (this.err_callback)
		this.parent.reject_was_handled = true;

	if (this.parent.value instanceof Promise)
	{
		this.parent = this.parent.value;

		if (this.parent.state === PromiseState.PENDING)
			this.parent.pending_then.push(this);
		else
			this.exec();
		return;
	}

	try
	{
		this.child.value = callback(this.parent.value);
	}
	catch (ex)
	{
		this.child.state = PromiseState.REJECTED;
		this.child.value = ex;
	}

	if (this.child.state === PromiseState.PENDING)
		this.child.state = PromiseState.FULFILLED;

	for (let idx in this.child.pending_then)
	{
		if (!this.child.pending_then.hasOwnProperty(idx))
			continue;

		this.child.pending_then[idx].exec();
	}

	if (this.child.state === PromiseState.REJECTED && !this.child.reject_was_handled)
		throw this.child.value;
}

let Promise = function(resolve_callback)
{
	this.state = PromiseState.PENDING;
	this.value = null;
	this.pending_then = [];
	this.reject_was_handled = false;

	let handle_ret = () => {
		for (let idx in this.pending_then)
		{
			if (!this.pending_then.hasOwnProperty(idx))
				continue;

			this.pending_then[idx].exec();
		}

		if (this.state === PromiseState.REJECTED && !this.reject_was_handled)
			throw this.value;
	};

	try
	{
		resolve_callback((val) => {
			window.setTimeout(() => {
				this.state = PromiseState.FULFILLED;
				this.value = val;
				handle_ret();
			}, 0);
		}, (reason) => {
			window.setTimeout(() => {
				this.state = PromiseState.REJECTED;
				this.value = reason;
				handle_ret();
			}, 0);
		});
	}
	catch (ex)
	{
		window.setTimeout(() => {
			this.state = PromiseState.REJECTED;
			this.value = ex;
			handle_ret();
		}, 0);
	}
}

Promise.resolve = function(val)
{
	return new Promise((resolve, reject) => resolve(val));
}

Promise.reject = function(reason)
{
	return new Promise((resolve, reject) => reject(reason));
}

Promise.all = function(deps)
{
	let cnt = 0;
	let result = [];
	let rejected = false;

	if (deps.length === 0)
		return Promise.resolve([]);

	return new Promise((resolve, reject) => {
		for (let i in deps)
		{
			let dep = deps[i];

			if (!(dep instanceof Promise))
				dep = Promise.resolve(dep);

			dep.then((val) => {
				result[i] = val;
				cnt++;

				if (cnt === deps.length)
					resolve(result);
			}, (reason) => {
				if (rejected)
					return;

				rejected = true;
				reject(reason);
			});
		}
	});
}

Promise.prototype.then = function(callback, err_callback)
{
	let child = new Promise(() => {});
	let block_then = new PromiseThen(this, child, callback, err_callback);

	if (this.state !== PromiseState.PENDING)
	{
		window.setTimeout(() => {
			block_then.exec();
		}, 0);
		return child;
	}

	this.pending_then.push(block_then);
	return child;
}

Promise.prototype.catch = function(err_callback)
{
	return this.then(null, err_callback);
}

Grim.Promise = (typeof window.Promise === 'undefined') ? Promise : window.Promise;

})();

(function() {

/* Imports */

let Promise = Grim.Promise;

/* AJAX */

let AJAX = function(method, url)
{
	let rq = new XMLHttpRequest();

	return new Promise((resolve, reject) => {
		rq.onreadystatechange = (evt) => {
			if (rq.readyState !== XMLHttpRequest.DONE)
				return;

			if (rq.status !== 200)
			{
				reject('AJAX request failed with HTTP status ' + rq.status + ' ' + rq.statusText);
				return;
			}

			resolve(rq.responseText);
		}

		rq.open(method, url, true);
		rq.send();
	});
}

/* Exports */

Grim.AJAX = AJAX;

})();

(function() {

/* Imports */

let Promise = Grim.Promise;

/* Utils */

let Utils = function() {};

Utils.getMouseRelativePos = function(elem, evt)
{
	let rect = elem.getBoundingClientRect();

	return {
		x: (evt.clientX - rect.left) / rect.width,
		y: (evt.clientY - rect.top) / rect.height
	};
}

Utils.getTimestamp = function()
{
	return new Date().getTime();
}

Utils.loadImg = function(url)
{
	let img = new Image();

	return new Promise((resolve, reject) => {
		img.addEventListener('load', () => resolve(img));
		img.addEventListener('error', () => reject('Loading of image "' + img.src + '" failed.'));
		img.addEventListener('abort', () => reject('Loading of image "' + img.src + '" aborted.'));
		img.src = url;
	});
}

Utils.loadImgAuto = function(url)
{
	return Utils.loadImg(url).then((img) => {
		img.width = img.naturalWidth;
		img.height = img.naturalHeight;

		return img;
	});
}

Utils.loadVideo = function(url, attrs)
{
	let video = document.createElement('video');
	let wait_complete_load = false;

	for (let i in attrs)
	{
		let attr = attrs[i];

		if (!attrs.hasOwnProperty(i))
			continue;

		if (i === 'Grim.WaitCompleteLoad')
		{
			wait_complete_load = attr;
			continue;
		}

		video.setAttribute(i, attr);
	}

	return new Promise((resolve, reject) => {
		video.addEventListener(wait_complete_load ? 'canplaythrough' : 'canplay', () => resolve(video));
		video.addEventListener('error', () => reject('Loading of video "' + video.src + '" failed.'));
		video.addEventListener('abort', () => reject('Loading of video "' + video.src + '" aborted.'));
		video.src = url;
	});
}

Utils.loadVideoAuto = function(url, attrs)
{
	return Utils.loadVideo(url, attrs).then((video) => {
		video.width = video.videoWidth;
		video.height = video.videoHeight;

		return video;
	});
}

Utils.disableDragDrop = function(elem)
{
	let handler = (evt) => {
		evt.preventDefault();
	};

	elem.addEventListener('drag', handler);
	elem.addEventListener('dragstart', handler);
	elem.addEventListener('dragover', handler);
	elem.addEventListener('dragleave', handler);
	elem.addEventListener('dragexit', handler);
	elem.addEventListener('dragenter', handler);
	elem.addEventListener('dragend', handler);
}

Utils.disableUserSelect = function(elem)
{
	let style = elem.getAttribute('style');

	if (style === undefined || style === null)
		style = '';

	style += '-webkit-touch-callout: none;';
	style += '-webkit-user-select: none;';
	style += '-khtml-user-select: none;';
	style += '-moz-user-select: none;';
	style += '-ms-user-select: none;';
	style += '-o-user-select: none;';
	style += 'user-select: none;';

	elem.setAttribute('style', style);
}

Utils.requestPointerLock = function(elem)
{
	if (elem.requestPointerLock)
	{
		elem.requestPointerLock();
		return;
	}
	
	if (elem.setCapture)
	{
		elem.setCapture(false);
		return;
	}

	console.log('libJSGrim: Utils.requestPointerLock() not implemented.');
};

Utils.exitPointerLock = function()
{
	if (document.exitPointerLock)
	{
		document.exitPointerLock();
		return;
	}
	
	if (document.releaseCapture)
	{
		document.releaseCapture();
		return;
	}

	console.log('libJSGrim: Utils.exitPointerLock() not implemented.');
};

Utils.updateStyle = function(elem, key, val)
{
	let style = elem.getAttribute('style');
	let found = false;

	if (!style)
		style = '';

	let props_arr = style.split(';');
	for (let i in props_arr)
	{
		let prop = props_arr[i];

		if (!props_arr.hasOwnProperty(i))
			continue;

		let prop_key = prop.split(':', 2)[0];
		if (prop_key !== key)
			continue;

		props_arr[i] = key + ':' + val;
		found = true;
		break;
	}

	style = props_arr.join(';');

	if (!found)
	{
		style = style.trim();

		if (style.length > 0
				&& style[style.length - 1] !== ';')
			style += ';';

		style += key + ':' + val + ';';
	}

	elem.setAttribute('style', style);
}

/* Exports */

Grim.Utils = Utils;

})();

(function() {

/* SVG */

let SVG = function() {};

SVG.createElement = function(tag_name)
{
	return document.createElementNS('http://www.w3.org/2000/svg', tag_name);
}

SVG.createSVGElement = function()
{
	return SVG.createElement('svg');
}

SVG.strToSVGElement = function(str)
{
	let div = document.createElement('div');
	let svg;

	div.innerHTML = str;
	svg = div.getElementsByTagName('svg')[0];

	if (svg === undefined || svg === null)
		throw 'Malformed SVG';

	return svg;
}

/* SVGSVGElement */

let SVGElement = function() {}

SVGElement.setRect = function(elem, x, y, w, h)
{
	elem.setAttribute('x', x);
	elem.setAttribute('y', y);
	elem.setAttribute('width', w);
	elem.setAttribute('height', h);
}

/* Exports */

SVG.SVGElement = SVGElement;
Grim.SVG = SVG;

})();

