(function() {

/* Imports */

let Promise = Grim.Promise;

/* AJAX */

let AJAX = function(method, url)
{
	let rq = new XMLHttpRequest();

	return new Promise((resolve, reject) => {
		rq.onreadystatechange = (evt) => {
			if (rq.readyState !== XMLHttpRequest.DONE)
				return;

			if (rq.status !== 200)
			{
				reject('AJAX request failed with HTTP status ' + rq.status + ' ' + rq.statusText);
				return;
			}

			resolve(rq.responseText);
		}

		rq.open(method, url, true);
		rq.send();
	});
}

/* Exports */

Grim.AJAX = AJAX;

})();

